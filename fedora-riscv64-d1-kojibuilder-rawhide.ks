# Kickstart file for Fedora RISC-V (riscv64) Developer Rawhide with XFCE desktop and lightdm

#repo --name="koji-override-0" --baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/

#repo --name="test-fedora-origin" --baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/
#repo --name="test-fedora-origin" --baseurl=http://openkoji.iscas.ac.cn/pub/rocks-mirror/rawhide/latest/riscv64/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=fedora-riscv
rootpw --plaintext fedora_rocks!
firewall --enabled --ssh
timezone --utc Asia/Shanghai
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd,haveged,kojid --disabled=lm_sensors,libvirtd,gdm

bootloader --location=none --extlinux

#zerombr
clearpart --all --initlabel --disklabel=gpt
part swap --asprimary --label=swap --size=32
part /boot/efi --fstype=vfat --size=128 --label=EFI
part /boot --fstype=ext4 --size=512 --label=boot
part / --fstype=ext4 --size=12288 --label=rootfs

# Halt the system once configuration has finished.
poweroff

%packages
@core
@hardware-support
-initial-setup-gui
-glibc-all-langpacks
-iproute-tc
-trousers
-xkeyboard-config
-usb_modeswitch
-ipw*
-iwl*

kernel
kernel-core
kernel-modules
linux-firmware
extlinux-bootloader
uboot-tools
freedom-u540-c000-bootloader
# Remove this in %post
dracut-config-generic
-dracut-config-rescue

glibc-langpack-en
nano
chrony
haveged
qemu-guest-agent

-grubby
grubby-deprecated

# No longer in @core since 2018-10, but needed for livesys script
initscripts
chkconfig

wget

# Lets resize / on first boot
#dracut-modules-growroot

koji-builder
appliance-tools

%end


%post

echo 'nameserver 114.114.114.114' >> /etc/resolv.conf

pushd /tmp

/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/dl-kernel-cross/kernel-cur.tar"

pushd boot
/usr/bin/tar  xpf kernel-cur.tar
popd

mv -vf boot/lib/modules/* /lib/modules/
mv -vi boot/boot-5.4.img /boot
mv -vi boot/*-5.4.61* /boot

rm -r boot

popd

%end

%post
# Disable default repositories (not riscv64 in upstream)
dnf config-manager --set-disabled rawhide updates updates-testing fedora fedora-modular fedora-cisco-openh264 updates-modular updates-testing-modular rawhide-modular

dnf -y remove dracut-config-generic

# systemd on no-SMP boots (i.e. single core) sometimes timeout waiting for storage
# devices. After entering emergency prompt all disk are mounted.
# For more information see:
# https://www.suse.com/support/kb/doc/?id=7018491
# https://www.freedesktop.org/software/systemd/man/systemd.mount.html
# https://github.com/systemd/systemd/issues/3446
# We modify /etc/fstab to give more time for device detection (the problematic part)
# and mounting processes. This should help on systems where boot takes longer.
sed -i 's|noatime|noatime,x-systemd.device-timeout=300s,x-systemd.mount-timeout=300s|g' /etc/fstab

# remove unnecessary entry in /etc/fstab

sed -i '/swap/d' /etc/fstab
sed -i '/efi/d' /etc/fstab

# Fedora 31
# https://fedoraproject.org/wiki/Changes/DisableRootPasswordLoginInSshd
cat > /etc/rc.d/init.d/livesys << EOF
#!/bin/bash
#
# live: Init script for live image
#
# chkconfig: 345 00 99
# description: Init script for live image.
### BEGIN INIT INFO
# X-Start-Before: display-manager chronyd
### END INIT INFO

. /etc/rc.d/init.d/functions

useradd -c "Fedora RISCV User" riscv
echo fedora_rocks! | passwd --stdin riscv > /dev/null
usermod -aG wheel riscv > /dev/null

exit 0
EOF

chmod 755 /etc/rc.d/init.d/livesys
/sbin/restorecon /etc/rc.d/init.d/livesys
/sbin/chkconfig --add livesys

# Create Fedora RISC-V repo
cat << EOF > /etc/yum.repos.d/fedora-riscv.repo
[fedora-riscv]
name=Fedora RISC-V
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/
enabled=1
gpgcheck=0

[fedora-riscv-debuginfo]
name=Fedora RISC-V - Debug
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/riscv64/debug/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/riscv64/debug/
enabled=0
gpgcheck=0

[fedora-riscv-source]
name=Fedora RISC-V - Source
baseurl=http://fedora.riscv.rocks/repos-dist/rawhide/latest/src/
#baseurl=https://dl.fedoraproject.org/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
#baseurl=https://mirror.math.princeton.edu/pub/alt/risc-v/repo/fedora/rawhide/latest/src/
enabled=0
gpgcheck=0
EOF

# Create Fedora RISC-V Koji repo
cat << EOF > /etc/yum.repos.d/fedora-riscv-koji.repo
[fedora-riscv-koji]
name=Fedora RISC-V Koji
baseurl=http://fedora.riscv.rocks/repos/rawhide/latest/riscv64/
enabled=0
gpgcheck=0
EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the Fedora/RISC-V disk image
https://fedoraproject.org/wiki/Architectures/RISC-V

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is 'fedora_rocks!'.
root password logins are disabled in SSH starting Fedora 31.
User 'riscv' with password 'fedora_rocks!' in 'wheel' group is provided.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/fedora-riscv.repo’.

For updates and latest information read:
https://fedoraproject.org/wiki/Architectures/RISC-V

Fedora/RISC-V
-------------
Koji:               http://fedora.riscv.rocks/koji/
SCM:                http://fedora.riscv.rocks:3000/
Distribution rep.:  http://fedora.riscv.rocks/repos-dist/
Koji internal rep.: http://fedora.riscv.rocks/repos/
EOF

# Remove machine-id on pre generated images
rm -f /etc/machine-id
touch /etc/machine-id

# remove random seed, the newly installed instance should make it's own
rm -f /var/lib/systemd/random-seed

%end


%post --nochroot

/usr/sbin/sfdisk -l /dev/loop0

echo 'Deleting the first partition of Image...'
/usr/sbin/sfdisk --delete /dev/loop0 1
/usr/sbin/sfdisk -l /dev/loop0

echo 'Reorder the partitions of Image...'
/usr/sbin/sfdisk -r /dev/loop0
/usr/sbin/sfdisk -l /dev/loop0

pushd /tmp
/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/d1-firmware/boot0_sdcard_sun20iw1p1.bin"
/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/d1-firmware/boot_package.fex"

dd if=boot/boot0_sdcard_sun20iw1p1.bin of=/dev/loop0 seek=16 bs=512
dd if=boot/boot_package.fex of=/dev/loop0 seek=32800 bs=512

popd

%end

# EOF
